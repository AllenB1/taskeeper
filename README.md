# taskkeeper
Keeps tasks for you so that you don't have to juggle them around in your head.

## Contributing
Suggestions & bugs go to issues.
Make PRs for implementations to a suggestion or fix to a bug